from settings import*
from mini_game_sprites import*

os.chdir(RESOURCES_DIR)
map_file = open("map1.txt", "r")
MAP_STRING = map_file.read()
map_file.close()

class Game:
    def __init__(self):
        self.game_width, self.game_height = 989, PORTRAIT_HEIGHT
        self.screen = pg.display.set_mode((self.game_width, self.game_height))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = True
        self.retry = False
        pg.font.init()
        pg.mixer.init()
        self.ouch_sound = pg.mixer.Sound("male_grunt3.wav")
        self.pick_up_sound = pg.mixer.Sound("button-19.wav")
        self.thunder_sound = pg.mixer.Sound("Thunder.wav")
        if PRECIPITATION == "Rain":
            self.BGM = "sfx_rain_3_extended.mp3"
            pg.mixer.music.load(self.BGM.replace("mp3", "ogg").replace("m4a", "ogg"))
            pg.mixer.music.play()


        self.initial_health = PLAYER_HEALTH_MAX
        self.initial_stamina = PLAYER_STAMINA_MAX
        self.health = PLAYER_HEALTH_MAX
        self.stamina = PLAYER_STAMINA_MAX


    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.moving_platforms = pg.sprite.Group()
        self.sand_sprites = pg.sprite.Group()
        self.falling_platforms = pg.sprite.Group()
        self.precipitation_sprites = pg.sprite.Group()
        self.drops = pg.sprite.Group()
        self.goal = pg.sprite.Group()

        self.background = Static(0, self.game_height-1900, "rocky_cliff_bg.png")
        self.all_sprites.add(self.background)


        map_string_split = MAP_STRING.strip().split("\n")
        map_string_split.reverse()
        base_y = self.game_height + 23*5
        for i in range(len(map_string_split)):
            row = map_string_split[i]
            for j in range(len(row)):
                char = row[j]
                if char == "D":
                    self.door = Static(j*23, base_y - 23*i - 40, "sprite_castledoors.png")
                    self.all_sprites.add(self.door)
                    self.goal.add(self.door)
                elif char == "S":
                    self.player = Player(self, j*23, base_y- 23*i - 50)
                    self.all_sprites.add(self.player)
                elif char == "G":
                    drop = Drops(j*23, base_y - 23*i, "Gold", "sprite_gold.png")
                    self.all_sprites.add(drop)
                    self.drops.add(drop)
                elif char == "B":
                    drop = Drops(j*23, base_y - 23*i, "Elderberry", "sprite_elderberry.png")
                    self.all_sprites.add(drop)
                    self.drops.add(drop)
                elif char == "R":
                    tile = Tile_Rock(j*23, base_y - 23*i)
                    self.all_sprites.add(tile)
                    self.platforms.add(tile)
                elif char == "A":
                    tile = Tile_Sand(j*23, base_y - 23*i)
                    self.all_sprites.add(tile)
                    self.sand_sprites.add(tile)

                ### complete the "elif" statements below to generate various types of tiles for letters/characters of your choice
                elif char == "":
                    tile = Tile_Ice(j*23, base_y - 23*i)
                    self.all_sprites.add(tile)
                    self.platforms.add(tile)
                    self.falling_platforms.add(tile) #because ice tiles are a special type of platform, we also have to
                                                     #add it to a pre-defined "falling_platforms" group so that
                                                     #the code will execute logic to make the ice tiles fall if you
                                                     #stand on it too long
                elif char == "":
                    tile = Tile_Snow(j*23, base_y - 23*i)
                    self.all_sprites.add(tile)
                    self.platforms.add(tile)
                elif char == "":
                    tile = Tile_Earth(j*23, base_y - 23*i)
                    self.all_sprites.add(tile)
                    self.platforms.add(tile)




        self.hp_mp_frame = Static(30, 30, "bar_hp_mp.png")
        self.hp_bar = Dynamic(30, 30, "bar_hp.png", "Health", self)
        self.mp_bar = Dynamic(30, 30 + 16, "bar_mp.png", "Stamina", self)
        self.all_sprites.add(self.hp_mp_frame)
        self.all_sprites.add(self.hp_bar)
        self.all_sprites.add(self.mp_bar)

        self.white_screen = Static(-3000,-3000,"sprite_white_1200_600.png")
        self.all_sprites.add(self.white_screen)


        self.gold = 0
        self.screen_bottom = self.game_height
        self.screen_bottom_difference = self.game_height - self.player.last_height
        self.last_precipitation = 0
        self.last_thunder = 0
        self.last_flash = 0
        self.thundering = False


        if not self.retry:
            self.show_start_screen()
        self.run()


    def run(self):
        while self.playing:
            try:
                if self.running:
                    self.clock.tick(FPS)
                    self.events()
                    self.update()
                    self.draw()
            except Exception as e:
                pass


    def update(self):

        self.all_sprites.update()

        # PLATFORM COLLISION
        on_screen_platforms = [platform for platform in self.platforms if platform.rect.left >= 0 and platform.rect.right <= self.game_width]
        collision = pg.sprite.spritecollide(self.player, on_screen_platforms, False)

        # MOVING PLATFORM COLLISION
        moving_platform_collision = pg.sprite.spritecollide(self.player, self.moving_platforms, False)

        # SAND COLLISION
        sand_collision = pg.sprite.spritecollide(self.player, self.sand_sprites, False)

        if collision:
            if (self.player.vel.y > self.player.player_height // 2) or \
                    (self.player.vel.y > 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height * 3 // 5) or \
                    (self.player.vel.y < 0 and self.player.rect.bottom - collision[
                        0].rect.top <= self.player.player_height // 2):
                self.player.pos.y = collision[0].rect.top
                self.player.vel.y = 0
            elif self.player.vel.y < 0:
                self.player.pos.y = collision[0].rect.bottom + self.player.player_height
                self.player.vel.y = 0

            if self.player.vel.y == 0:
                for col in collision:
                    if self.player.rect.bottom - col.rect.top <= self.player.player_height * 3 // 5 and self.player.pos.y > col.rect.top:
                        self.player.pos.y = col.rect.top
                        self.player.vel.y = 0

            current_height = collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 450:
                damage = int(200 * 1.005 ** (fall_height - 450))
                self.take_damage(damage)
                print("Ouch! Lost {} health from fall.".format(damage))

            self.player.last_height = current_height
            self.player.jumping = False

            if type(collision[0]) in [Tile_Snow, Tile_Ice]:
                self.player.friction = PLAYER_FRICTION/4

        else:
            self.player.friction = PLAYER_FRICTION


        for col in on_screen_platforms:
            if self.player.vel.x > 0 and self.player.pos.x >= col.rect.left - self.player.player_width * 1.1 and \
                    self.player.pos.x <= col.rect.left and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x
            elif self.player.vel.x < 0 and self.player.pos.x <= col.rect.right + self.player.player_width * 1.1 and \
                    self.player.pos.x >= col.rect.right and \
                    self.player.pos.y - col.rect.top <= self.player.player_height and \
                    self.player.pos.y - col.rect.top >= self.player.player_height / 2:
                self.player.pos.x -= self.player.vel.x
                self.player.absolute_pos -= self.player.vel.x

        if sand_collision:
            self.player.vel.y *= .5
            current_height = sand_collision[0].rect.top
            fall_height = current_height - self.player.last_height
            if fall_height >= 600:
                damage = int(200 * 1.005 ** (fall_height - 600))
                print("Ouch! Lost {} health from fall.".format(damage))
                self.take_damage(damage)

            self.player.last_height = current_height
            self.player.jumping = False


        # GOAL COLLISION
        goal_collision = pg.sprite.spritecollide(self.player, self.goal, False)
        if goal_collision:
            self.playing = False
            self.running = False
            self.show_game_win_screen()


        #WINDOW SCROLLING
        if self.player.rect.top <= self.game_height // 3:
            self.player.pos.y += abs(self.player.vel.y)
            self.player.last_height += abs(self.player.vel.y)
            for platform in self.platforms:
                platform.rect.y += abs(self.player.vel.y)
                if type(platform) == Tile_Ice:
                    platform.dissolve_height += abs(self.player.vel.y)
            for platform in self.moving_platforms:
                platform.y_range[0] += abs(self.player.vel.y)
                platform.y_range[1] += abs(self.player.vel.y)
            for item in self.goal:
                item.rect.y += abs(self.player.vel.y)
            for drop in self.drops:
                drop.rect.y += abs(self.player.vel.y)
            for sand in self.sand_sprites:
                sand.rect.y += abs(self.player.vel.y)

            self.background.rect.y += abs(self.player.vel.y/4)
            self.screen_bottom += abs(self.player.vel.y)

        elif self.player.rect.bottom >= self.game_height * 2 // 3 and self.player.vel.y > 0 and self.player.rect.top <= self.screen_bottom - self.screen_bottom_difference:
            self.player.last_height -= abs(self.player.vel.y)
            self.player.pos.y -= abs(self.player.vel.y)
            for platform in self.platforms:
                platform.rect.y -= abs(self.player.vel.y)
                if type(platform) == Tile_Ice:
                    platform.dissolve_height -= abs(self.player.vel.y)
            for platform in self.moving_platforms:
                platform.y_range[0] -= abs(self.player.vel.y)
                platform.y_range[1] -= abs(self.player.vel.y)
            for item in self.goal:
                item.rect.y -= abs(self.player.vel.y)
            for drop in self.drops:
                drop.rect.y -= abs(self.player.vel.y)
            for sand in self.sand_sprites:
                sand.rect.y -= abs(self.player.vel.y)

            self.background.rect.y -= abs(self.player.vel.y / 4)
            self.screen_bottom -= abs(self.player.vel.y)

        # FALLING PLATFORM COLLISION
        falling_platform_collision = pg.sprite.spritecollide(self.player, self.falling_platforms, False)
        if falling_platform_collision:
            falling_platform_collision[0].total_standing_time += 1
        else:
            self.reset_falling_platforms()


        # DROP COLLISION
        drop_collision = pg.sprite.spritecollide(self.player, self.drops, True, pg.sprite.collide_circle_ratio(.75))
        if drop_collision:
            drop = drop_collision[0]
            self.pick_up_drop(drop)


        #SANK TO BOTTOM
        if self.player.rect.top >= self.screen_bottom*1.5:
            self.playing = False
            self.running = False
            self.show_game_over_screen()


        if self.stamina < PLAYER_STAMINA_MAX:
            self.stamina += 1
        if self.health < PLAYER_HEALTH_MAX:
            self.health += 1

        now = pg.time.get_ticks()
        if now - self.last_precipitation > 200 and PRECIPITATION:
            self.last_precipitation = now
            self.generate_precipitation()

        if self.thundering:
            time_since_thunder = now - self.last_thunder
            if time_since_thunder <= 900:
                if random() <= .5:
                    self.white_screen.rect.x = 0
                    self.white_screen.rect.y = 0
                else:
                    self.white_screen.rect.x = -3000
                    self.white_screen.rect.y = -3000

            elif time_since_thunder >= 5900:
                self.thundering = False

            elif self.last_flash < self.last_thunder:
                self.white_screen.rect.x = -3000
                self.white_screen.rect.y = -3000
                self.thunder_sound.play()
                self.last_flash = now

        else:
            if now - self.last_thunder > randrange(18000, 36000):
                self.last_thunder = now
                self.thundering = True


    def generate_precipitation(self):
        try:
            if self.playing and self.running:
                for i in range(PRECIPITATION_AMOUNT):
                    random_x = randrange(int(self.game_width*1.1))
                    random_y = randrange(-200,-100)
                    if PRECIPITATION == "Rain":
                        random_falling_speed = randrange(11, 16)
                        p = Rain(random_x, random_y, random_falling_speed, self)
                    elif PRECIPITATION == "Snow":
                        random_falling_speed = randrange(3, 8)
                        p = Snow(random_x, random_y, random_falling_speed, self)
                    self.all_sprites.add(p)
                    self.precipitation_sprites.add(p)
        except Exception as e:
            pass


    def events(self):
        for event in pg.event.get():

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump()

            if event.type == pg.KEYUP:
                if event.key == pg.K_UP and self.player.stun_count == 0:
                    self.player.jump_cut()


    def take_damage(self, damage):
        self.ouch_sound.play()
        self.health -= damage
        if self.health <= 0:
            self.health = 0
            self.playing = False
            self.running = False
            self.show_game_over_screen()


    def pick_up_drop(self, drop):
        if drop.name == "Gold":
            self.gold += 100
        elif drop.name == "Elderberry":
            self.stamina = PLAYER_STAMINA_MAX
        self.pick_up_sound.play()


    def reset_falling_platforms(self, value=0):
        for fp in self.falling_platforms:
            fp.total_standing_time = value


    def draw(self):
        if self.running and self.playing:
            self.screen.fill(BLACK)
            self.all_sprites.draw(self.screen)
            self.draw_text("Gold: {}".format(self.gold), 16, WHITE, 920, 20)
            pg.display.flip()


    def show_start_screen(self):
        self.draw_text("Use the arrow keys to move/jump.", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press any key to begin.", 22, WHITE, self.game_width // 2, self.game_height // 2)
        pg.display.flip()
        self.listen_for_key()


    def show_game_over_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You died!", 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to retry or 'q' to quit.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def show_game_win_screen(self):
        self.dim_screen = pg.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0,0,0,200))
        self.screen.blit(self.dim_screen, (0,0))
        self.draw_text("You won! Your final score is: {}.".format(self.gold), 22, WHITE, self.game_width//2, self.game_height//3)
        self.draw_text("Press 'r' to play again or 'q' to quit.", 22, WHITE, self.game_width // 2, self.game_height // 2)

        pg.display.flip()
        self.listen_for_key(False)


    def listen_for_key(self, start=True): #if not start, then apply game over logic
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():

                if event.type == pg.KEYUP:
                    if start:
                        waiting = False
                        self.running = True
                    else:
                        if event.key == pg.K_r:
                            waiting = False
                            self.running = True
                            self.playing = True
                            self.retry = True
                            self.health = int(self.initial_health)
                            self.stamina = int(self.initial_stamina)
                            self.new()
                        elif event.key == pg.K_q:
                            waiting = False
                            self.running = False
                            self.playing = False
                            pg.quit()


    def draw_text(self, text, size, color, x, y):
        font = pg.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        self.screen.blit(text_surface,text_rect)



if __name__ == "__main__":
    g = Game()
    g.new()
    pg.quit()