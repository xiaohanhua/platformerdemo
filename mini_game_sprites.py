from math import atan, pi
from settings import*
from random import*
import os
import pygame as pg



vec = pg.math.Vector2
#os.chdir(os.path.join(os.getcwd(), RESOURCES_DIR))


class Player(pg.sprite.Sprite):

    def __init__(self, game, x, y):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.image = pg.image.load("sprite_main_char_stand.png")
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.center = (self.game.game_width/2, self.game.game_height/2)
        self.player_width = self.image.get_width()
        self.player_height = self.image.get_height()


        self.current_frame = 0
        self.last_update = 0
        self.direction = "Right"
        self.moving = False
        self.standing_on_moving_platform = False
        self.jumping = False
        #self.jump_velocity = (self.game.main_game.speed + self.game.main_game.dexterity)//30 + 16
        self.jump_velocity = JUMP_VELOCITY
        self.friction = PLAYER_FRICTION
        self.stun_count = 0



        self.pos = vec(x, y)
        self.vel = vec(0, 0)
        self.acc = vec(0, 0)
        self.last_height= self.pos.y
        self.absolute_pos = x
        self.load_animations()



    def load_animations(self):
        self.walk_frames_r = [pg.image.load("sprite_main_char_run{}.png".format(i)) for i in range(1,9)]
        self.walk_frames_l = [pg.transform.flip(im, True, False) for im in self.walk_frames_r]
        self.walk_frames_u = [pg.image.load("sprite_main_char_stand.png")]
        self.walk_frames_d = [pg.image.load("sprite_main_char_stand.png")]

        self.animation_frames = [self.walk_frames_r,
                                 self.walk_frames_l,
                                 self.walk_frames_u,
                                 self.walk_frames_d]

        for frames_list in self.animation_frames:
            for frame in frames_list:
                frame.set_colorkey(BLACK)



    def animate(self, frame_delta = 100):
        directions = ["Right", "Left", "Up", "Down"]
        direction_index = directions.index(self.direction)
        now = pg.time.get_ticks()

        if self.jumping:
            if self.vel.y < 0:
                self.image = pg.image.load("sprite_main_char_jump_up.png")
                self.image.set_colorkey(RED)

            else:
                self.image = pg.image.load("sprite_main_char_jump_down.png")
                self.image.set_colorkey(BLACK)

            if self.vel.x < 0:
                self.image = pg.transform.flip(self.image, True, False)

            if now - self.last_update > frame_delta:
                if self.stun_count > 0:
                    if self.stun_count%2:
                        self.image = pg.image.load("sprite_transparent.png")
                    self.stun_count -= 1


        elif not self.moving:
            if self.stun_count == 0:
                self.image = pg.image.load("sprite_main_char_stand.png")
                self.image.set_colorkey(BLACK)
                if self.vel.x < 0:
                    self.image = pg.transform.flip(self.image, True, False)

            elif now - self.last_update > frame_delta:
                self.last_update = now
                self.stun_count -= 1
                if self.stun_count%2:
                    self.image = pg.image.load("sprite_transparent.png")
                else:
                    self.image = pg.image.load("sprite_main_char_stand.png")


        else:
            if now - self.last_update > frame_delta:
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.animation_frames[direction_index])
                self.image = self.animation_frames[direction_index][self.current_frame]

                if self.current_frame%2 and self.stun_count > 0:
                    self.image = pg.image.load("sprite_transparent.png")
                    self.stun_count -= 1


    def jump_cut(self):
        if self.vel.y <= -2 and (not self.standing_on_moving_platform or self.jumping):
            self.vel.y = -2


    def jump(self):
        if self.game.stamina < STAMINA_EXPENDITURE:
            return
        standing_on_platform = pg.sprite.spritecollide(self, self.game.platforms, False)
        standing_in_sand = pg.sprite.spritecollide(self, self.game.sand_sprites, False)
        if standing_on_platform and not self.jumping:
            self.jumping = True
            self.vel.y = -self.jump_velocity
            self.game.stamina -= STAMINA_EXPENDITURE
        elif standing_in_sand and not self.jumping:
            self.jumping = True
            self.vel.y = -self.jump_velocity//4


    def update(self):
        self.animate()
        standing_in_sand = pg.sprite.spritecollide(self, self.game.sand_sprites, False)
        self.acc = vec(0, GRAVITY)
        if standing_in_sand:
            self.acc.y *= .5
        keys = pg.key.get_pressed()
        self.moving = False
        if keys[pg.K_LEFT] and self.stun_count == 0:
            self.acc.x = -PLAYER_ACC
            self.direction = "Left"
            self.moving = True
        if keys[pg.K_RIGHT] and self.stun_count == 0:
            self.acc.x = PLAYER_ACC
            self.direction = "Right"
            self.moving = True

        if standing_in_sand:
            self.acc.x *= .2
            self.acc.y += self.vel.y * self.friction
        self.acc.x += self.vel.x*self.friction
        self.vel += self.acc
        self.pos += self.vel + .5*self.acc
        self.rect.center = self.pos
        self.absolute_pos += self.vel.x

        if self.pos.x > self.game.game_width - self.player_width*.5:
            self.pos.x = self.game.game_width - self.player_width*.5
        if self.pos.x <  self.player_width*.5:
            self.pos.x =  self.player_width*.5

        self.rect.midbottom = self.pos



class Ant(pg.sprite.Sprite):

    def __init__(self, game, x, y):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.image = pg.image.load("sprite_ant_up_1.png")
        self.rect = self.image.get_rect()
        self.rect.center = (PORTRAIT_WIDTH/2, PORTRAIT_HEIGHT/2)
        self.player_width = self.image.get_width()
        self.player_height = self.image.get_height()

        self.current_frame = 0
        self.last_update = 0
        self.direction = "Up"
        self.friction = PLAYER_FRICTION
        self.moving = False

        self.pos = vec(x, y)
        self.vel = vec(0, 0)
        self.acc = vec(0, 0)
        self.load_animations()

    def load_animations(self):
        self.walk_frames_u = [pg.image.load("sprite_ant_up_{}.png".format(i)) for i in range(1,3)]
        self.walk_frames_d = [pg.transform.flip(im, False, True) for im in self.walk_frames_u]
        self.walk_frames_l = [pg.transform.rotate(im, 90) for im in self.walk_frames_u]
        self.walk_frames_r = [pg.transform.rotate(im, 270) for im in self.walk_frames_u]

        self.animation_frames = [self.walk_frames_r,
                                 self.walk_frames_l,
                                 self.walk_frames_u,
                                 self.walk_frames_d]

    def animate(self, frame_delta = 100):
        if self.moving:
            directions = ["Right", "Left", "Up", "Down"]
            direction_index = directions.index(self.direction)
            now = pg.time.get_ticks()
            if now - self.last_update > frame_delta:
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.animation_frames[direction_index])
                self.image = self.animation_frames[direction_index][self.current_frame]

    def update(self):
        self.animate()
        keys = pg.key.get_pressed()
        self.acc.x = 0
        self.acc.y = 0
        self.moving = False

        if keys[pg.K_LEFT]:
            self.acc.x = -PLAYER_ACC
            self.direction = "Left"
            self.moving = True
        if keys[pg.K_RIGHT]:
            self.acc.x = PLAYER_ACC
            self.direction = "Right"
            self.moving = True
        if keys[pg.K_UP]:
            self.acc.y = -PLAYER_ACC
            self.direction = "Up"
            self.moving = True
        if keys[pg.K_DOWN]:
            self.acc.y = PLAYER_ACC
            self.direction = "Down"
            self.moving = True

        self.acc.x += self.vel.x*self.friction
        self.acc.y += self.vel.y*self.friction
        self.vel += self.acc
        self.pos += self.vel + .5*self.acc
        self.rect.center = self.pos

        if self.pos.x > self.game.game_width - self.player_width:
            self.pos.x = self.game.game_width - self.player_width
        if self.pos.x <  self.player_width:
            self.pos.x =  self.player_width
        if self.pos.y > self.game.game_height:
            self.pos.y = self.game.game_height
        if self.pos.y <  self.player_height:
            self.pos.y =  self.player_height

        self.rect.midbottom = self.pos



class Spider(pg.sprite.Sprite):

    def __init__(self, game, x, y, x_vel, y_vel):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.pos = vec(x, y)
        self.vel = vec(x_vel, y_vel)
        self.last_update = 0
        self.current_frame = 0
        self.load_animations()
        self.update_sprite()

        directions = ["Right", "Left", "Up", "Down"]
        direction_index = directions.index(self.direction)
        now = pg.time.get_ticks()
        self.current_frame = (self.current_frame + 1) % len(self.animation_frames[direction_index])
        self.image = self.animation_frames[direction_index][self.current_frame]

        self.rect = self.image.get_rect()
        self.rect.center = (self.game.game_width/2, self.game.game_height)
        self.player_width = self.image.get_width()
        self.player_height = self.image.get_height()

        self.friction = PLAYER_FRICTION


    def load_animations(self):
        self.walk_frames_d = [pg.image.load("sprite_spider_down_{}.png".format(i)) for i in range(1,3)]
        self.walk_frames_u = [pg.transform.flip(im, False, True) for im in self.walk_frames_d]
        self.walk_frames_l = [pg.image.load("sprite_spider_left_{}.png".format(i)) for i in range(1,3)]
        self.walk_frames_r = [pg.transform.flip(im, True, False) for im in self.walk_frames_l]

        self.animation_frames = [self.walk_frames_r,
                                 self.walk_frames_l,
                                 self.walk_frames_u,
                                 self.walk_frames_d]


    def update_sprite(self,frame_delta=100):
        if self.vel.x > 0:
            self.direction = "Right"
        elif self.vel.x < 0:
            self.direction = "Left"
        elif self.vel.y > 0:
            self.direction = "Down"
        elif self.vel.y < 0:
            self.direction = "Up"

        directions = ["Right", "Left", "Up", "Down"]
        direction_index = directions.index(self.direction)
        now = pg.time.get_ticks()
        if now - self.last_update > frame_delta:
            self.last_update = now
            self.current_frame = (self.current_frame + 1) % len(self.animation_frames[direction_index])
            self.image = self.animation_frames[direction_index][self.current_frame]


    def update(self):

        self.pos += self.vel
        self.rect.center = self.pos

        if random() <= .01:
            if self.vel.x != 0:
                self.vel.y = self.vel.x*1
                self.vel.x = 0
            else:
                self.vel.x = self.vel.y * 1
                self.vel.y = 0


        if self.pos.x > self.game.game_width - self.player_width:
            self.pos.x = self.game.game_width - self.player_width
            self.vel.x *= -1

        if self.pos.x <  self.player_width:
            self.pos.x =  self.player_width
            self.vel.x *= -1

        if self.pos.y > self.game.game_height:
            self.pos.y = self.game.game_height
            self.vel.y *= -1

        if self.pos.y <  self.player_height:
            self.pos.y =  self.player_height
            self.vel.y *= -1

        self.update_sprite()
        self.rect.midbottom = self.pos



class Platform_Earth(pg.sprite.Sprite):
    def __init__(self, x, y, w, h, vx=0, vy=0, x_range=(0,0), y_range=(0,0), moving=False):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load("tile_earth.png")
        self.image = pg.transform.scale(self.image, (w, h))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.vx = vx
        self.vy = vy
        self.x_range = x_range
        self.y_range = y_range
        self.moving = moving


    def update(self):
        if self.moving:
            self.rect.x += self.vx
            self.rect.y += self.vy
            if (self.rect.x >= self.x_range[1] and self.vx > 0) or (self.rect.x <= self.x_range[0] and self.vx < 0):
                self.vx *= -1
            if (self.rect.y >= self.y_range[1] and self.vy > 0) or (self.rect.y <= self.y_range[0] and self.vy < 0):
                self.vy *= -1



class Tile_Earth(pg.sprite.Sprite):
    def __init__(self, x, y):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load("tile_earth_block.png")
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


class Tile_Snow(pg.sprite.Sprite):
    def __init__(self, x, y):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load("tile_snow_block.png")
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


class Tile_Rock(pg.sprite.Sprite):
    def __init__(self, x, y):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load("tile_rock_block.png")
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y



class Tile_Sand(pg.sprite.Sprite):
    def __init__(self, x, y):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load("tile_sand_block.png")
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y



class Tile_Ice(pg.sprite.Sprite):
    def __init__(self, x, y):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load("tile_cracked_ice_block.png")
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.falling_speed = 0
        self.falling = False
        self.total_standing_time = 0
        self.dissolve_height = PORTRAIT_HEIGHT


    def update(self):
        if self.total_standing_time >= 7:
            self.falling = True
        if self.falling:
            self.falling_speed += GRAVITY*1.25
            self.rect.y += self.falling_speed
        if self.rect.y >= self.dissolve_height:
            self.kill()







class Ninja(pg.sprite.Sprite):
    def __init__(self, x, y, game, throwing=0, weapons=['Ninja Star'], reverse=False):
        pg.sprite.Sprite.__init__(self)
        self.left_image = pg.image.load("sprite_ninja_throw_star_1.png")
        self.right_image = pg.transform.flip(self.left_image, True, False)
        self.image = self.left_image
        self.image.set_colorkey(BLACK)
        if reverse:
            self.image = pg.transform.flip(self.image, True, False)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.pos = vec(x,y)
        self.rect.midbottom = self.pos
        self.game = game
        self.weapons = weapons

        self.current_frame = 0
        self.last_update = 0
        self.last_throw = 0
        self.throwing = throwing
        self.throwing_direction = -1

        self.load_animations()


    def update(self):
        if self.rect.x <= self.game.game_width*1.2 and self.rect.x > -20:
            self.animate()

        if self.game.player.rect.x > self.rect.right and self.throwing_direction < 0:
            self.change_direction()
        elif self.game.player.rect.x < self.rect.left and self.throwing_direction > 0:
            self.change_direction()


    def load_animations(self):
        self.animation_frames = [pg.image.load("sprite_ninja_throw_star_{}.png".format(i)) for i in range(1,7)]


    def change_direction(self):
        self.throwing_direction *= -1
        for i in range(len(self.animation_frames)):
            frame = self.animation_frames[i]
            self.animation_frames[i] = pg.transform.flip(frame, True, False)

    def animate(self, frame_delta = 100):
        now = pg.time.get_ticks()
        if not self.throwing:
            if self.throwing_direction < 0:
                self.image = self.left_image
            else:
                self.image = self.right_image

        else:
            if now - self.last_update > frame_delta:
                self.last_update = now
                self.current_frame = (self.current_frame + 1)%len(self.animation_frames)
                self.image = self.animation_frames[self.current_frame]
                if self.current_frame == len(self.animation_frames) - 1:
                    self.throwing = False
                elif self.current_frame == 4:
                    if self.throwing_direction < 0:
                        self.game.generate_projectile(self.rect.left, (self.rect.y + self.rect.centery)//2,
                                                           self.throwing_direction*randrange(8,13), self.weapons)
                    else:
                        self.game.generate_projectile(self.rect.right, (self.rect.y + self.rect.centery)//2,
                                                           self.throwing_direction*randrange(8,13), self.weapons)


        if now - self.last_throw > frame_delta*30:
            self.last_throw = now
            self.throwing = True




class Archer(pg.sprite.Sprite):
    def __init__(self, x, y, game, throwing=0, weapons=['Arrow'], reverse=False):
        pg.sprite.Sprite.__init__(self)
        self.left_image = pg.image.load("Sprite_Archer_1.png")
        self.right_image = pg.transform.flip(self.left_image, True, False)
        self.image = self.left_image
        self.image.set_colorkey(BLACK)
        if reverse:
            self.image = pg.transform.flip(self.image, True, False)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.pos = vec(x,y)
        self.rect.midbottom = self.pos
        self.game = game
        self.weapons = weapons

        self.current_frame = 0
        self.last_update = 0
        self.last_throw = 0
        self.throwing = throwing
        self.throwing_direction = -1

        self.load_animations()


    def update(self):
        if self.rect.x <= self.game.game_width*1.2 and self.rect.x > -20:
            self.animate()

        if self.game.player.rect.x > self.rect.right and self.throwing_direction < 0:
            self.change_direction()
        elif self.game.player.rect.x < self.rect.left and self.throwing_direction > 0:
            self.change_direction()


    def load_animations(self):
        self.animation_frames = [pg.image.load("Sprite_Archer_{}.png".format(i)) for i in range(1,11)]


    def change_direction(self):
        self.throwing_direction *= -1
        for i in range(len(self.animation_frames)):
            frame = self.animation_frames[i]
            self.animation_frames[i] = pg.transform.flip(frame, True, False)

    def animate(self, frame_delta = 50):
        now = pg.time.get_ticks()
        if not self.throwing:
            if self.throwing_direction < 0:
                self.image = self.left_image
            else:
                self.image = self.right_image

        else:
            if now - self.last_update > frame_delta:
                self.last_update = now
                self.current_frame = (self.current_frame + 1)%len(self.animation_frames)
                self.image = self.animation_frames[self.current_frame]
                if self.current_frame == len(self.animation_frames) - 1:
                    self.throwing = False
                elif self.current_frame == 8:
                    if self.throwing_direction < 0:
                        self.game.generate_projectile(self.rect.left, self.rect.centery - self.image.get_height()//5,
                                                           self.throwing_direction*randrange(15,21), self.weapons)
                    else:
                        self.game.generate_projectile(self.rect.right, self.rect.centery - self.image.get_height()//5,
                                                           self.throwing_direction*randrange(15,21), self.weapons)


        if now - self.last_throw > frame_delta*40:
            self.last_throw = now
            self.throwing = True





class Spearman(pg.sprite.Sprite):
    def __init__(self, x, y, game, weapons=[], reverse=False):
        pg.sprite.Sprite.__init__(self)
        self.left_image = pg.image.load("Sprite_Spearman_1.png")
        self.right_image = pg.transform.flip(self.left_image, True, False)
        self.image = self.left_image
        self.image.set_colorkey(BLACK)
        if reverse:
            self.image = pg.transform.flip(self.image, True, False)
        self.rect = self.image.get_rect()
        self.pos = vec(x,y)
        self.rect.bottomright = vec(x,y)
        self.game = game
        self.weapons = weapons

        self.current_frame = 0
        self.last_update = 0
        self.last_stab = 0
        self.stabbing = False
        self.stabbing_direction = -1
        self.stabbing_dangerous = False

        self.load_animations()


    def update(self):
        if self.rect.x <= self.game.game_width*1.2 and self.rect.x > -20:
            self.animate()

        if self.game.player.rect.x > self.rect.right and self.stabbing_direction < 0:
            self.change_direction()
        elif self.game.player.rect.x < self.rect.left and self.stabbing_direction > 0:
            self.change_direction()


        self.stabbing_detection()


    def load_animations(self):
        self.animation_frames = [pg.image.load("Sprite_Spearman_{}.png".format(i)) for i in range(1,9)]


    def change_direction(self):
        self.stabbing_direction *= -1
        for i in range(len(self.animation_frames)):
            frame = self.animation_frames[i]
            self.animation_frames[i] = pg.transform.flip(frame, True, False)


    def animate(self, frame_delta = 50):
        now = pg.time.get_ticks()
        if not self.stabbing:
            if self.stabbing_direction < 0:
                self.image = self.left_image
            else:
                self.image = self.right_image

        else:
            if now - self.last_update > frame_delta:
                self.last_update = now
                self.current_frame = (self.current_frame + 1)%len(self.animation_frames)
                self.image = self.animation_frames[self.current_frame]
                if self.current_frame == len(self.animation_frames) - 1:
                    self.stabbing = False
                    self.stabbing_dangerous = False
                if self.current_frame == 3:
                    self.stabbing_dangerous = True

        if now - self.last_stab > frame_delta*randrange(28,33):
            self.last_stab = now
            self.stabbing = True


    def stabbing_detection(self):
        if self.game.running and self.game.playing:
            if self.stabbing_direction < 0:
                condition_1 = abs(self.game.player.rect.centery - self.rect.centery) <= self.game.player.player_height // 2
                condition_2 = self.rect.left - self.game.player.rect.right <= 0
                if condition_1 and condition_2 and self.stabbing_dangerous:
                    self.stabbing_dangerous = False
                    self.game.take_damage(200)
                    self.game.player.stun_count += randrange(10,21)
                    if self.game.player.rect.left >= self.rect.centerx:
                        self.game.player.vel.x += 15
                    else:
                        self.game.player.vel.x -= 15

            else:
                condition_1 = abs(self.game.player.rect.centery - self.rect.centery) <= self.game.player.player_height // 2
                condition_2 = self.game.player.rect.left - self.rect.right <= 0
                if condition_1 and condition_2 and self.stabbing_dangerous:
                    self.stabbing_dangerous = False
                    self.game.take_damage(200)
                    self.game.player.stun_count += randrange(10,21)
                    if self.game.player.rect.left >= self.rect.centerx:
                        self.game.player.vel.x += 15
                    else:
                        self.game.player.vel.x  -= 15


class Monk(pg.sprite.Sprite):
    def __init__(self, x, y, game, line_of_sight=6):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load("sprite_monk_standing_left.png")
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.direction = "Left"
        self.pos = vec(x,y)
        self.rect.midbottom = self.pos
        self.game = game
        self.last_turned = 0
        self.line_of_sight = line_of_sight


    def update(self):
        now = pg.time.get_ticks()
        if now - self.last_turned > randrange(2500, 5000):
            self.change_direction()
            self.last_turned = now

        self.detect_player()


    def detect_player(self):
        if self.game.running and self.game.playing:
            if self.direction == "Left":
                condition_1 = self.rect.left - self.game.player.rect.right <= self.line_of_sight*23
                condition_2 = abs(self.game.player.rect.centery - self.rect.centery) <= self.game.player.player_height//2
                condition_3 = self.rect.left - self.game.player.rect.right > 0
                if condition_1 and condition_2 and condition_3 and random() >= .25*self.game.main_game.luck/50:
                    #print("Detected")
                    self.game.detected()
            else:
                condition_1 = self.game.player.rect.left - self.rect.right <= self.line_of_sight*23
                condition_2 = abs(self.game.player.rect.centery - self.rect.centery) <= self.game.player.player_height//2
                condition_3 = self.game.player.rect.left - self.rect.right > 0
                if condition_1 and condition_2 and condition_3 and random() >= .25*self.game.main_game.luck/50:
                    #print("Detected")
                    self.game.detected()


    def change_direction(self):
        self.image = pg.transform.flip(self.image, True, False)
        if self.direction == "Left":
            self.direction = "Right"
        else:
            self.direction = "Left"



class Projectile(pg.sprite.Sprite):
    def __init__(self, x, y, x_vel, y_vel, name, filename, damage, stun, colorkey=None):
        pg.sprite.Sprite.__init__(self)
        self.name = name
        self.image = pg.image.load(filename)
        if self.name == "Arrow":
            self.image = pg.transform.scale(self.image, (25,4))
        if colorkey:
            self.image.set_colorkey(colorkey)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.x_vel = x_vel
        self.y_vel = y_vel
        self.damage = damage
        self.stun = stun
        if self.name == "Poison Needle":
            if self.x_vel < 0:
                self.image = pg.transform.flip(self.image, True, False)
            self.set_orientation()


    def set_orientation(self):
        angle = atan(self.y_vel/self.x_vel)*180/pi
        self.image = pg.transform.rotate(self.image, -angle)


    def update(self):
        if self.x_vel > 0 and self.name not in ["Poison Needle", "Boulder"]:
            self.image = pg.transform.flip(self.image, True, False)
        self.rect.x += self.x_vel
        self.rect.y += self.y_vel




class Static_Enemy(pg.sprite.Sprite):
    def __init__(self, x, y, filename, reverse=False):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load(filename)
        if reverse:
            self.image = pg.transform.flip(self.image, True, False)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y



class Luohan_Monk(pg.sprite.Sprite):

    def __init__(self, x, y, game, damage, stationary=False):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.damage = damage
        self.image = pg.image.load("sprite_staff_monk_walk_left1.png")
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.bottom = y
        self.vel = 0
        self.acc = 0
        self.acc_multiplier = randrange(8,13)/10
        self.stationary = stationary

        self.player_width = self.image.get_width()
        self.player_height = self.image.get_height()
        self.direction = "Right"
        self.jumping = False
        self.moving = False

        self.current_frame = 0
        self.last_update = 0
        self.last_stab = 0
        self.last_stab_hit = -1000
        self.stabbing = False
        self.stabbing_direction = -1
        self.current_frame_stab = 0

        self.load_animations()



    def load_animations(self):
        self.walk_frames_l = [pg.image.load("sprite_staff_monk_walk_left{}.png".format(i)) for i in range(2,10)]
        self.walk_frames_r = [pg.transform.flip(im, True, False) for im in self.walk_frames_l]
        self.walk_frames_u = [pg.image.load("sprite_staff_monk_walk_left1.png")]
        self.walk_frames_d = [pg.image.load("sprite_staff_monk_walk_left1.png")]

        self.animation_frames = [self.walk_frames_r,
                                 self.walk_frames_l,
                                 self.walk_frames_u,
                                 self.walk_frames_d]

        for frames_list in self.animation_frames:
            for frame in frames_list:
                frame.set_colorkey(WHITE)

        self.animation_frames_stab_left = [pg.image.load("sprite_staff_monk_stab{}.png".format(i)) for i in range(1,6)]
        self.animation_frames_stab_right = [pg.transform.flip(img, True, False) for img in self.animation_frames_stab_left]


    def animate(self, frame_delta = 100):
        directions = ["Right", "Left", "Up", "Down"]
        direction_index = directions.index(self.direction)
        now = pg.time.get_ticks()
        if not self.moving:
            self.vel = 0
            self.stabbing = False
            self.current_frame_stab = 0
            self.image = pg.image.load("sprite_staff_monk_walk_left1.png")
            self.image.set_colorkey(WHITE)
            if self.direction == "Right":
                self.image = pg.transform.flip(self.image, True, False)

        else:
            self.stabbing = False
            if now - self.last_update > frame_delta:
                self.last_update = now
                self.current_frame = (self.current_frame + 1)%len(self.animation_frames[direction_index])
                self.image = self.animation_frames[direction_index][self.current_frame]


    def animate_stab(self, frame_delta=100):
        self.stabbing = True
        self.current_frame = 0
        now = pg.time.get_ticks()
        if now - self.last_stab > frame_delta:
            self.last_stab = now
            if self.current_frame_stab == 0:
                self.game.projectile_sound.play()
            if self.stabbing_direction > 0:
                self.current_frame_stab = (self.current_frame_stab + 1) % len(self.animation_frames_stab_right)
                self.image = self.animation_frames_stab_right[self.current_frame_stab]
            else:
                self.current_frame_stab = (self.current_frame_stab + 1) % len(self.animation_frames_stab_left)
                self.image = self.animation_frames_stab_left[self.current_frame_stab]


    def stabbing_detection(self):
        now = pg.time.get_ticks()
        if self.game.running and self.game.playing:
            if self.stabbing_direction < 0:
                condition_1 = abs(self.game.player.rect.centery - self.rect.centery) <= self.game.player.player_height // 2
                condition_2 = self.rect.left - self.game.player.rect.right <= 0
                if condition_1 and condition_2 and self.last_stab - self.last_stab_hit >= 750:
                    self.last_stab_hit = now
                    self.game.take_damage(self.damage)
                    self.game.player.stun_count += randrange(3,6)
                    if self.game.player.rect.left >= self.rect.centerx:
                        self.game.player.vel.x += 5
                    else:
                        self.game.player.vel.x -= 5

            else:
                condition_1 = abs(self.game.player.rect.centery - self.rect.centery) <= self.game.player.player_height // 2
                condition_2 = self.game.player.rect.left - self.rect.right <= 0
                if condition_1 and condition_2  and self.last_stab - self.last_stab_hit >= 750:
                    self.last_stab_hit = now
                    self.game.take_damage(self.damage)
                    self.game.player.stun_count += randrange(3,6)
                    if self.game.player.rect.left >= self.rect.centerx:
                        self.game.player.vel.x += 5
                    else:
                        self.game.player.vel.x  -= 5


    def pick_direction(self):
        r = random()
        condition_1 = self.rect.centerx - self.game.player.pos.x >= 30 and self.direction == "Right"
        condition_2 = self.rect.centerx - self.game.player.pos.x <= -30 and self.direction == "Left"
        condition_3 = abs(self.rect.centerx - self.game.player.pos.x) <= 50 or abs(self.rect.centerx - self.game.player.pos.x) >= 300
        condition_4 = abs(self.rect.centery - self.game.player.pos.y) >= self.game.player.image.get_height()*3
        if condition_1 or condition_2:
            if self.direction == "Left":
                self.direction = "Right"
                self.stabbing_direction = 1
            else:
                self.direction = "Left"
                self.stabbing_direction = -1

        if not (condition_3 or condition_4) or (abs(self.rect.centerx - self.game.player.pos.x) >= 300 and random() <= .15):
            self.acc = PLAYER_ACC*(self.acc_multiplier if self.direction == "Right" else - self.acc_multiplier)
            self.moving = True


    def update(self):
        if self.rect.right <= -30 or self.rect.left >= self.game.game_width + 30:
            return
        self.acc = 0
        self.moving = False
        self.pick_direction()

        if not self.stationary:
            self.acc += self.vel * PLAYER_FRICTION
            self.vel += self.acc
            if self.direction == "Left":
                self.rect.right += .5*self.vel
            else:
                self.rect.left += .5*self.vel

            if abs(self.rect.centerx - self.game.player.pos.x) <= 50:
                self.animate_stab()
                self.stabbing_detection()
            else:
                self.animate()

        else:
            self.animate_stab()
            self.stabbing_detection()


class Static(pg.sprite.Sprite):
    def __init__(self, x, y, filename):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load(filename)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y



class Drops(pg.sprite.Sprite):
    def __init__(self, x, y, name, filename):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load(filename)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.name = name



class Dynamic(pg.sprite.Sprite):
    def __init__(self, x, y, filename, sprite_type, game):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.filename = filename
        self.image = pg.image.load(self.filename)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.sprite_type = sprite_type

    def update(self):
        self.image = pg.image.load(self.filename)
        if self.sprite_type == "Health":
            self.image = pg.transform.scale(self.image, (int(self.game.health / PLAYER_HEALTH_MAX * 102), 14))
        elif self.sprite_type == "Stamina":
            self.image = pg.transform.scale(self.image, (int(self.game.stamina / PLAYER_STAMINA_MAX * 102), 14))




class Rain(pg.sprite.Sprite):
    def __init__(self, x, y, falling_speed, game):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.falling_speed = falling_speed
        self.image = pg.Surface((1,randrange(3,6))).convert_alpha()
        self.image.fill(GRAY)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def update(self):
        self.rect.y += self.falling_speed
        if self.rect.y >= self.game.game_height*1.5 or self.rect.x <= -200:
            self.kill()


class Snow(pg.sprite.Sprite):
    def __init__(self, x, y, falling_speed, game):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        self.falling_speed = falling_speed
        self.image = pg.image.load("sprite_snow.png")
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def update(self):
        self.rect.y += self.falling_speed
        if self.rect.y >= self.game.game_height * 1.5 or self.rect.x <= -200:
            self.kill()